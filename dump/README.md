# sys_mssi_64_cn_armv82-user 13 TP1A.220905.001 1681528831759 release-keys
- manufacturer: oplus
- platform: mt6781
- codename: ossi
- flavor: sys_mssi_64_cn_armv82-user
- release: 13
- id: TP1A.220905.001
- incremental: 1681528831759
- tags: release-keys
- fingerprint: oplus/ossi/ossi:12/SP1A.210812.016/1681820780934:user/release-keys
- is_ab: false
- brand: oplus
- branch: sys_mssi_64_cn_armv82-user-13-TP1A.220905.001-1681528831759-release-keys
- repo: oplus_ossi_dump
